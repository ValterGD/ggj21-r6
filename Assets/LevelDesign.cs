﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDesign : MonoBehaviour
{
	private void OnEnable()
	{
		EnableRandomLevel();
	}

	public void EnableRandomLevel() 
	{
		int randomIndex = Random.Range(0, transform.childCount);

		for (int i = 0; i < transform.childCount; i++)
		{
			transform.GetChild(i).gameObject.SetActive(i == randomIndex);
		}
	}
}
