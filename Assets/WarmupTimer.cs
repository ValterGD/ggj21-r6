﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using DarkTonic.MasterAudio;

public class WarmupTimer : MonoBehaviour
{
	[Header("General Warmup")]
	[SerializeField] float preWarmup = 3;
	[SerializeField] int warmupTimer = 3;
	[SerializeField] TextMeshProUGUI remainingTime;
	[SerializeField] CanvasGroup timerCg;

	[Header("Light TurnOn")]
	[SerializeField] LightCone lightScript;
	[SerializeField] GameObject lightObject;
	[SerializeField] float lightStopedTime = 1;

	private void Start()
	{
		StartCoroutine(RunTimer());
	}

	public IEnumerator RunTimer() 
	{
		yield return new WaitForSeconds(preWarmup);

		remainingTime.transform.localScale = Vector3.one;
		timerCg.alpha = 1;
		for (int i = warmupTimer; i > 0; i--)
		{
			remainingTime.text = "" + i;
			IntroTextFeedback();
			yield return new WaitForSeconds(1);
		}

		IntroTextFeedback(3);
		lightObject.SetActive(true);
		remainingTime.text = "GO!";
		MasterAudio.PlaySound("StartMatch");
		yield return new WaitForSeconds(0.7f);
		remainingTime.transform.DOScale(new Vector3(0, 0, 0), 0.3f).SetEase(Ease.InBack);
		lightScript.enabled = true;
	}

	public void IntroTextFeedback(float punchForce = 1) 
	{
		if (punchForce == 1)
			MasterAudio.PlaySound("CountBump");

		remainingTime.transform.DOPunchRotation(new Vector3(0, 0, 45), 0.3f).SetEase(Ease.OutBack);
		remainingTime.transform.DOPunchScale(new Vector3(0.3f, 0.3f) * punchForce, 0.3f).SetEase(Ease.OutBack);
	}
}
