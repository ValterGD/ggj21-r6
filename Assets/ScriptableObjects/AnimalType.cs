﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class AnimalType : ScriptableObject
{
    public Transform prefab;
    public bool available;  
}
