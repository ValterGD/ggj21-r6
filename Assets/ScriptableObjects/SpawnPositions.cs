﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SpawnPositions : ScriptableObject
{
    public List<Vector3> spawnPoints;
}
