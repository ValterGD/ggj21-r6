﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayerManager : ScriptableObject
{
    public List<PlayerStatus> players = new List<PlayerStatus>();
}
