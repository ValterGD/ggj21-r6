﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class AnimalManager : ScriptableObject
{
    public List<AnimalType> animalType;
}
