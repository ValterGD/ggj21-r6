﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerStatus
{
    public bool isActive;
    public bool isAlive;
    public Transform transform;
    public bool isReady;
    public bool menu;
    public Transform animal;
    public PlayerInput Input;
}
