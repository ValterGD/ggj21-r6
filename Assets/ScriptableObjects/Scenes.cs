﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu]
public class Scenes : ScriptableObject
{
    public string sceneName;
    public SpawnPositions spawnPositions;
}
