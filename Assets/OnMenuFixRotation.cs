﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class OnMenuFixRotation : MonoBehaviour 
{
	private void OnEnable()
	{
		FixRotation(true);
	}

	public void FixRotation(bool inverse = false)
	{
		transform.GetChild(0).transform.localEulerAngles = new Vector3(0, inverse ? 180 : 0, 0);
	}
	
}