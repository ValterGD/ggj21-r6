﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DeathTimer : MonoBehaviour
{
	[SerializeField] Image timerFill;
	[SerializeField] CanvasGroup cg;

	float lastTimer;
	bool timerEnabled;
	bool alreadyDead;

	public void StartTimer(float time)
	{
		if (!timerEnabled && !alreadyDead)
		{
			cg.DOFade(1, time / 8).SetId(gameObject);		

			timerFill.color = Color.green;
			timerFill.fillAmount = 0;

			timerFill.DOColor(Color.red, time - time / 8).SetId(gameObject).SetEase(Ease.Linear);
			timerFill.DOFillAmount(1, time).SetId(gameObject).SetEase(Ease.Linear).OnComplete(() => { StopTimer(); alreadyDead = true; });

			timerEnabled = true;
			lastTimer = time;
		}
	}

	public void StopTimer()
	{
		DOTween.Pause(gameObject);

		cg.DOFade(0, lastTimer / 8).SetId(gameObject);
		timerEnabled = false;
	}

	public void ResetDead()
    {
		alreadyDead = false;
	}
}
