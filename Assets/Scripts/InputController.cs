﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

public class InputController : MonoBehaviour
{
    PlayerInput playerInput;
    InputAction bButton;
    PlayerSetup playerSetup;

    Vector2 movementInput;
    Movement movement;

    public float dashDelay = 2f;

    private bool isAlive = true;

    float dashTimer = 0;
    bool canDash = true;

    bool menu = false;

    bool movedLeft = false;
    bool movedRight = false;


    void Awake()
    {
        playerInput = GetComponent<PlayerInput>();
        movement = GetComponent<Movement>();
        playerSetup = GetComponent<PlayerSetup>();

        bButton = playerInput.actions["B"];
        bButton.performed += StartB;
        bButton.canceled += StopB;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (isAlive && !menu)
        {
            movement.SetMovement(movementInput);

            if (dashTimer > dashDelay)
            {
                canDash = true;
            }
            else dashTimer += Time.deltaTime;
            if (movement.GetIsDashing()) dashTimer = 0;
        }
        else if (menu)
        {
            if (movementInput.x > 0.8f && !movedRight)
            {
                playerSetup.NextAnimal();
                movedRight = true;
            }
            else if (movementInput.x < -0.8f && !movedLeft)
            {
                playerSetup.PreviousAnimal();
                movedLeft = true;
            }
            else if (Mathf.Abs(movementInput.x) < 0.5f)
            {
                movedLeft = false;
                movedRight = false;
            }
        }
    }

    void FixedUpdate()
    {

    }

    private void OnMove(InputValue value)
    {
        movementInput = value.Get<Vector2>();
    }

    private void OnB()
    {
        if (!menu)
        {
            if (canDash)
            {
                movement.Dash();
                dashTimer = 0;
                canDash = false;
            }
        }
        else if (playerSetup.GetIsReady())
        {
            playerSetup.Unready();
        }
        else
        {
            playerSetup.Unjoin();
        }
    }

    private void OnA()
    {
        if (menu)
        {
            playerSetup.ReadyPlayer();
        }
    }

    void StartB(CallbackContext ctx)
    {

    }

    void StopB(CallbackContext ctx)
    {

    }

    public void Death()
    {
        isAlive = false;
    }

    public void Alive()
    {
        isAlive = true;
    }

    public void SetMenu(bool b)
    {
        menu = b;
    }

    public void OnDestroy()
    {

    }

    void OnDeviceLost()
    {
        playerSetup.Unjoin();
        Object.Destroy(transform.parent.gameObject);
    }
}
