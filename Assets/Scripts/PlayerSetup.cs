﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerSetup : MonoBehaviour
{
    [SerializeField] DeathTimer deathTimer;
    [Space]

    public AnimalManager animalManager;
    public PlayerManager playerManager;
    public SpawnPositions spawnPositions;
    public SpawnPositions used;


    InputController inputController;
    PlayerInput playerInput;
    Rigidbody rBody;
    Movement movement;
    Animator animator;

    PlayerStatus playerStatus = new PlayerStatus();

    int animalTypeIndex = 0;

    float timeInside = 0;
    float maxTimeInside = 1f;

    private bool menu = true;
    private bool isActive = true;

    private bool isAlive = true;

    private int positionIndex = 0;

    List<Collider> lightColliders = new List<Collider>();

    private void Awake()
    {
        rBody = GetComponent<Rigidbody>();
        inputController = GetComponent<InputController>();
        playerInput = GetComponent<PlayerInput>();
        movement = GetComponent<Movement>();
        animator = transform.GetChild(0).GetComponent<Animator>();
        animator.SetBool("waiting", true);

        playerStatus.isReady = false;
        playerStatus.isActive = true;
        playerStatus.isAlive = true;
        playerStatus.transform = transform;
        playerStatus.Input = playerInput;
        playerStatus.menu = true;

        animalTypeIndex = 0;
        Debug.Log(animalManager.animalType.Count);
        foreach (AnimalType at in animalManager.animalType)
        {
            if (at.available)
            {
                playerStatus.animal = at.prefab;
                at.available = false;
                break;
            }
            animalTypeIndex++;
        }

        Transform animalGraphic = Instantiate(playerStatus.animal, Vector3.zero, Quaternion.identity);
        animalGraphic.SetParent(transform.GetChild(0), false);


        foreach (Vector3 position in spawnPositions.spawnPoints)
        {
            if (!used.spawnPoints.Contains(position))
            {
                transform.position = position;
                positionIndex = spawnPositions.spawnPoints.IndexOf(position);
                used.spawnPoints.Add(position);
                break;
            }
        }

        playerManager.players.Add(playerStatus);
        SetMenu(true);
    }

    private void Update()
    {
        if (!menu && playerStatus.menu)
        {
            menu = true;
            SetMenu(true);
            Debug.Log("Saiu do menu");
        }
        else if (menu && !playerStatus.menu)
        {
            menu = false;
            SetMenu(false);
        }

        if (!isActive && playerStatus.isActive)
        {
            FreePlayer();
            isActive = true;
            timeInside = 0;
            deathTimer.StopTimer();
            lightColliders.Clear();

        }
        else if (isActive && !playerStatus.isActive)
        {
            LockPlayer();
            isActive = false;
        }

        if (!isAlive && playerStatus.isAlive)
        {
            movement.Alive();
            inputController.Alive();
            animator.SetTrigger("doLive");
            isAlive = true;
            deathTimer.ResetDead();
            lightColliders.Clear();
            deathTimer.StopTimer();
        }

        if (isAlive && !playerStatus.isAlive)
        {
            isAlive = false;
        }

        if (lightColliders.Count > 0 && playerStatus.isActive && isAlive)
        {
            if(timeInside == 0) deathTimer.StartTimer(maxTimeInside);
            timeInside += Time.deltaTime;
            if (timeInside > maxTimeInside)
            {
                playerStatus.isAlive = false;
                isAlive = false;
                movement.Death();
                inputController.Death();
            }
        }
        if (timeInside > 0 && lightColliders.Count <= 0 || !playerStatus.isActive)
        {
            timeInside = 0;
            deathTimer.StopTimer();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Light"))
        {
            if (!lightColliders.Contains(other))
            {
                lightColliders.Add(other);
            }
        }

        if (other.CompareTag("Car"))
        {
            playerStatus.isAlive = false;
            isAlive = false;
            movement.Death();
            inputController.Death();
            deathTimer.StopTimer();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Light"))
        {
            lightColliders.Remove(other);
        }
    }

    public void NextAnimal()
    {
        if (!playerStatus.isReady)
        {
            Object.Destroy(transform.GetChild(0).GetChild(0).gameObject);

            int index = animalTypeIndex;

            for (int i = 0; i < animalManager.animalType.Count; i++)
            {
                animalTypeIndex++;
                if (animalTypeIndex >= animalManager.animalType.Count) animalTypeIndex = 0;
                if (animalManager.animalType[animalTypeIndex].available)
                {
                    playerStatus.animal = animalManager.animalType[animalTypeIndex].prefab;
                    animalManager.animalType[animalTypeIndex].available = false;
                    animalManager.animalType[index].available = true;
                    break;
                }
            }

            Transform animalGraphic = Instantiate(playerStatus.animal, Vector3.zero, Quaternion.identity);
            animalGraphic.SetParent(transform.GetChild(0), false);
        }
    }

    public void PreviousAnimal()
    {
        if (!playerStatus.isReady)
        {
            Object.Destroy(transform.GetChild(0).GetChild(0).gameObject);

            int index = animalTypeIndex;

            for (int i = 0; i < animalManager.animalType.Count; i++)
            {
                animalTypeIndex--;
                if (animalTypeIndex < 0) animalTypeIndex = animalManager.animalType.Count - 1;
                if (animalManager.animalType[animalTypeIndex].available)
                {
                    playerStatus.animal = animalManager.animalType[animalTypeIndex].prefab;
                    animalManager.animalType[animalTypeIndex].available = false;
                    animalManager.animalType[index].available = true;
                    break;
                }
            }

            Transform animalGraphic = Instantiate(playerStatus.animal, Vector3.zero, Quaternion.identity);
            animalGraphic.SetParent(transform.GetChild(0), false);
        }
    }

    public void Unjoin()
    {
        used.spawnPoints.Remove(spawnPositions.spawnPoints[positionIndex]);
        playerStatus.isReady = false;
        playerManager.players.Remove(playerStatus);
        Object.Destroy(transform.parent.gameObject);
        animalManager.animalType[animalTypeIndex].available = true;
    }

    public void Unready()
    {
        playerStatus.isReady = false;
        animator.SetBool("waiting", true);
    }

    public void ReadyPlayer()
    {
        playerStatus.isReady = true;
        animator.enabled = true;
        animator.SetBool("waiting", false);
    }

    public void SetMenu(bool b)
    {
        menu = b;
            
        inputController.SetMenu(b);

        if (b)
        {
            LockPlayer();
        }
        else
        {
            FreePlayer();
        }
    }

    public void LockPlayer()
    {
        rBody.constraints = RigidbodyConstraints.None;
        rBody.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        rBody.transform.forward = Vector3.back;
    }

    public void FreePlayer()
    {
        rBody.constraints = RigidbodyConstraints.None;
        rBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionY;
        animator.SetBool("waiting", false);
        Debug.Log("Player Freed");
    }

    public bool GetIsReady()
    {
        return playerStatus.isReady;
    }
}
