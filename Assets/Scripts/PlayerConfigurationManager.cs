﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerConfigurationManager : MonoBehaviour
{
    public PlayerManager playerManager;

    public Scenes myScene;
    public SpawnPositions used;

    float timer = 0;

    bool gameStarted = false;

    private void Awake()
    {
        used.spawnPoints.Clear();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameStarted)
        {
            if (CountReadyPlayers() > 1 && CountReadyPlayers() == playerManager.players.Count)
            {
                timer += Time.deltaTime;
                if (timer > 2)
                {
                    StartGame();
                    gameStarted = true;
                }
            }
            else
            {
                timer = 0;
            }
        }
    }

    int CountReadyPlayers()
    {
        int count = 0;

        foreach (PlayerStatus ps in playerManager.players)
        {
            if (ps.isReady)
            {
                count++;
            }
        }

        return count;
    }

    public void StartGame()
    {
        foreach (PlayerStatus ps in playerManager.players)
        {
            ps.menu = false;
            ps.isAlive = true;
            ps.isActive = true;
            ps.isReady = true;
            DontDestroyOnLoad(ps.transform.parent);
        }

        SceneManager.LoadScene(myScene.sceneName);
        SetPlayerPositions(myScene);

    }

    void SetPlayerPositions(Scenes sc)
    {
        for (int i = 0; i < playerManager.players.Count; i++)
        {
            playerManager.players[i].transform.position = sc.spawnPositions.spawnPoints[playerManager.players.Count];
            playerManager.players[i].transform.GetComponentInChildren<OnMenuFixRotation>(true).FixRotation();
        }
    }

    public bool GetGameStarted()
    {
        return gameStarted;
    }
}
