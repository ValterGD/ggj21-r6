﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayers : MonoBehaviour
{
    public PlayerManager playerManager;
    public List<Transform> targets;

    private Vector3 velocity;
    public float smoothTime;

    public float zoomLimiter = 50f;
    public float minZoom = 40f;
    public float maxZoom = 10f;

    void LateUpdate()
    {
        Vector3 centralPosition = GetCenterPoint();
        transform.position = Vector3.SmoothDamp(transform.position, centralPosition + Camera.main.transform.forward * -50, ref velocity, smoothTime);
        Zoom();
    }

    Vector3 GetCenterPoint()
    {
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);

        foreach (PlayerStatus ps in playerManager.players)
        {
            if (ps.isActive && ps.isAlive)
            {
                bounds.Encapsulate(ps.transform.position);
            }
        }
        return bounds.center;
    }

    void Zoom()
    {
        float newZoom = Mathf.Lerp(maxZoom, minZoom, GetGreatestDistance() / zoomLimiter);
        Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, newZoom, Time.deltaTime);
    }

    float GetGreatestDistance()
    {
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);

        foreach (PlayerStatus ps in playerManager.players)
        {
            if (ps.isActive && ps.isAlive)
            {
                bounds.Encapsulate(ps.transform.position);
            }
        }
        return bounds.size.x;
    }
}
