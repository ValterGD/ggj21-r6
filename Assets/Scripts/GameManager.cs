﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using DarkTonic.MasterAudio;

public class GameManager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI victory;
    [SerializeField] TextMeshProUGUI endText;

    public PlayerManager playerManager;
    public AnimalManager animalManager;

    public Transform victoryParticle;

    public Scenes myScene;

    private bool gameEnded = false;

    float victoryTimer = 0;


    private void Awake()
    {
        InitializeAnimalManager();
    }

    // Start is called before the first frame update
    void Start()
    {
        victory.text = "";
        endText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameEnded)
        {
            if (CountAlivePlayers() <= 1)
            {
                victoryTimer += Time.deltaTime;
                if (victoryTimer > 1)
                {
                    gameEnded = true;
                    Debug.Log("Acabou a cena");
                    //Tem que fazer o reset aqui
                    Victory();
                }
            }
        }
    }

    void InitializeAnimalManager()
    {
        foreach (AnimalType at in animalManager.animalType)
        {
            at.available = true;
        }
    }

    int CountAlivePlayers()
    {
        int count = 0;

        foreach (PlayerStatus ps in playerManager.players)
        {
            if (ps.isAlive)
            {
                count++;
            }
        }

        Debug.Log("Alive players: " + count.ToString());
        return count;
    }

    void Victory()
    {
        PlayerStatus winner = null;
        foreach (PlayerStatus ps in playerManager.players)
        {
            if (ps.isAlive)
            {
                winner = ps;

                winner.transform.position = Vector3.zero;
                Instantiate(victoryParticle, winner.transform.position, Quaternion.identity);
            }

            ps.isActive = false;
        }
        if (winner != null)
        {
            victory.text = "Victory!";
            MasterAudio.PlaySound("Applause");
            endText.text = $"Player {playerManager.players.IndexOf(winner) + 1} runs away from the police!";

        }
        else
        {
            victory.text = "Draw!";
            endText.text = "The police caught everyone!";


        }
        StartCoroutine(RestartMatch());
    }

    IEnumerator RestartMatch()
    {
        yield return new WaitForSeconds(5);
        foreach (PlayerStatus ps in playerManager.players)
        {
            ps.menu = false;
            ps.isAlive = true;
            ps.isActive = true;
            ps.isReady = true;
            DontDestroyOnLoad(ps.transform.parent);
        }

        SceneManager.LoadScene(myScene.sceneName);
        SetPlayerPositions(myScene);
    }

    void SetPlayerPositions(Scenes sc)
    {
        for (int i = 0; i < playerManager.players.Count; i++)
        {
            playerManager.players[i].transform.position = sc.spawnPositions.spawnPoints[playerManager.players.Count];
        }
    }
}
