﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Movement : MonoBehaviour
{
    public float movementSpeed;
    public float maxRotation;

    public float deathSmooth = 1;

    public float dashForce;

    public bool absoluteMovement;

    Animator animator;


    Vector2 movementVec;
    Vector2 direction;
    Rigidbody rBody;

    private bool isAlive = true;

    Vector2 velocity2D;

    private Vector2 velocity = Vector2.zero;

    private bool isDashing;
    public float dashDuration;

    void Awake()
    {
        rBody = GetComponent<Rigidbody>();
        UnityEngine.Random.InitState(DateTime.UtcNow.Second);
        animator = GetComponentInChildren<Animator>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Players"))
        {
            if (collision.collider.transform.GetComponent<Movement>().GetIsDashing())
            {
                Vector3 flyingDirection = (transform.position - collision.collider.gameObject.transform.position).normalized;
                rBody.AddForce(flyingDirection * 1 * dashForce, ForceMode.VelocityChange);
                animator.SetTrigger("doDash");
                StartCoroutine(Dashing());
            }
            else
            {
                Vector3 flyingDirection = (transform.position - collision.collider.gameObject.transform.position).normalized;
                rBody.AddForce(flyingDirection * 0.2f * dashForce, ForceMode.VelocityChange);
                //animator.SetTrigger("doDash");
                StartCoroutine(Dashing());
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
            float angle = UnityEngine.Random.Range(0, 360) * Mathf.Deg2Rad;
            direction = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)).normalized;
            rBody.velocity = new Vector3(direction.x, 0, direction.y).normalized * movementSpeed;
    }

    void FixedUpdate()
    {
            
            if(new Vector2(rBody.velocity.x, rBody.velocity.z).sqrMagnitude > 0.5f) direction = new Vector2(rBody.velocity.x, rBody.velocity.z);
            if (!isDashing)
            {
                if (isAlive)
                {
                    if (absoluteMovement)
                    {
                        float angle = Vector2.SignedAngle(direction, -movementVec);

                        if (movementVec.sqrMagnitude > 0.8f)
                        {
                            if (angle >= 0)
                            {
                                direction = Rotate(direction, -maxRotation * Mathf.Rad2Deg);
                            }
                            else
                            {
                                direction = Rotate(direction, maxRotation * Mathf.Rad2Deg);
                            }
                        }
                    }

                    else
                    {
                        if (movementVec.x > 0.8f)
                        {
                            direction = Rotate(direction, -maxRotation * Mathf.Rad2Deg);
                        }
                        else if (movementVec.x < -0.8f)
                        {
                            direction = Rotate(direction, maxRotation * Mathf.Rad2Deg);
                        }
                    }

                    rBody.velocity = new Vector3(direction.x, 0, direction.y).normalized * movementSpeed;
                }
                else
                {
                    rBody.velocity = new Vector3(Mathf.SmoothStep(rBody.velocity.x, 0, deathSmooth), 0, Mathf.SmoothStep(rBody.velocity.y, 0, deathSmooth));
                }
            }
            if (rBody.velocity.normalized.sqrMagnitude > 0.5f) transform.forward = rBody.velocity.normalized;
    }

    public Vector2 Rotate(Vector2 v, float degrees)
    {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);
        return v;
    }

    public void SetMovement(Vector2 movement)
    {
        movementVec = movement.normalized;
    }


    public void Dash()
    {
        if (isAlive)
        {
            rBody.AddForce(transform.forward * dashForce, ForceMode.VelocityChange);
            animator.SetTrigger("doDash");
            StartCoroutine(Dashing());
        }
    }

    public void Death()
    {
        if (isAlive)
        {
            isAlive = false;
            animator.SetTrigger("doDeath");
        }
    }

    public void Alive()
    {
        if (!isAlive)
        {
            isAlive = true;
        }
    }

    IEnumerator Dashing()
    {
        yield return new WaitForSeconds(0);
        isDashing = true;
        yield return new WaitForSeconds(dashDuration);
        isDashing = false;
        rBody.velocity = transform.forward * movementSpeed;
    }

    public bool GetIsDashing()
    {
        return isDashing;
    }
}
