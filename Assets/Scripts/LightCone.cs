﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LightCone : MonoBehaviour
{
    [Header("Behavior")]
    public float lightSpeed;
    public float rotationSpeed;
    public float range;
    public int maxDuration;
    public int lightStart;
    public int lightEnd;

    [Header("Light Render")]
    [SerializeField] Light lightComponent;
    [SerializeField] float endRange = 600;

    float startRange;

    private float timer;

    Vector2 direction;

    int randomSelector;
    float positionTimer = 0;


    // Start is called before the first frame update
    void Start()
    {
        float angle = UnityEngine.Random.Range(0, 360) * Mathf.Deg2Rad;
        direction = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)).normalized;

        UnityEngine.Random.InitState(DateTime.UtcNow.Second);
        startRange = lightComponent.range;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        positionTimer += Time.deltaTime;
        if (positionTimer > 5f)
        {
            positionTimer = 0;
            randomSelector = UnityEngine.Random.Range(0, 2);
        }


        if (new Vector2(transform.position.x, transform.position.z).sqrMagnitude > range)
        {
            direction = -1 * direction;
        }

        if (randomSelector == 0)
        {
            direction = Rotate(direction, -rotationSpeed * Mathf.Rad2Deg);
        }
        else if (randomSelector == 1)
        {
            direction = Rotate(direction, rotationSpeed * Mathf.Rad2Deg);
        }

        transform.Translate(lightSpeed * new Vector3(direction.x, 0, direction.y) * Time.deltaTime);

        transform.position = new Vector3(transform.position.x, Mathf.Lerp(lightStart, lightEnd, timer/maxDuration), transform.position.z);
        lightComponent.range = Mathf.Lerp(startRange, endRange, timer / maxDuration);
    }

    public Vector2 Rotate(Vector2 v, float degrees)
    {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);
        return v;
    }
}
