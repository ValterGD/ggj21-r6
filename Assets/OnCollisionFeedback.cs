﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class OnCollisionFeedback : MonoBehaviour
{
	bool inAnimation;

	private void OnCollisionEnter(Collision collision)
	{
		if (!inAnimation)
		{
			gameObject.transform.DOPunchScale(new Vector3(0.2f,0.2f,0.2f), 0.4f).SetEase(Ease.OutBack).OnComplete(() => { inAnimation = false; });
			inAnimation = true;
		}
	}
}
